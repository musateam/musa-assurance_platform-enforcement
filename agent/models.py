from __future__ import unicode_literals

from django.db import models
from django.utils import timezone
from enforcement.models import Endpoint

#from jsonfield import JSONField
from jsoneditor.fields.django_json_field import JSONField
from jsonfield import JSONField as JSONField2

# Create your models here.
class Agent(models.Model):
    IDM = 'IDentityManager'
    ACONTROL = 'AccessControl'
    AGENT_TYPE = (
        (IDM, 'IDentityManager'),
        (ACONTROL, 'AccessControl'),
    )
    name = models.CharField(max_length=200, default='name')
    endpoint = models.ForeignKey(Endpoint, related_name='endpoint', on_delete=models.CASCADE, null=True)
    state = models.CharField(max_length=200, default='initial')
    agent_type = models.CharField(max_length=20, choices=AGENT_TYPE, default=IDM)
    create_date = models.DateTimeField('Agent created date', default=timezone.now)
    topic = models.CharField(max_length=200, default='musa.agent.default')
    system = JSONField2(max_length=50000, default=None)
    #settings = models.TextField(max_length=5000, default='{}')
    #settings = JSONField(max_length=50000, load_kwargs={'object_pairs_hook': collections.OrderedDict})
    settings = JSONField(max_length=50000)
    lastseen = models.DateTimeField('Last keep alive received', default=timezone.now)

    def __str__(self):
        return self.id

    def __unicode__(self):
        return '%s' % (self.id)


class Entry(models.Model):
    timestamp = models.DateTimeField('entry created date', default=timezone.now)
    category = models.CharField(max_length=20)
    action = models.CharField(max_length=20)
    tag = models.CharField(max_length=20)

    class Meta:
        abstract = True
        ordering = ['timestamp']

class AgentEntry(Entry):
    agent = models.ForeignKey(Agent, related_name='%(class)s_related', on_delete=models.CASCADE)
    topic = models.CharField(max_length=100, default='musa-agents-default')
    partition = models.IntegerField(default=0)
    class Meta:
        abstract = True

class StateEntry(AgentEntry):        
    state = models.CharField('state', max_length=100, default='initial')

    def __str__(self):
        return self.id

    def __unicode__(self):
        return '%s' % (self.id)

class ActivityEntry(AgentEntry):        
    #activity = models.CharField('activity', max_length=1000, default='activity')
    activity = JSONField(max_length=50000)

    def __str__(self):
        return self.id

    def __unicode__(self):
        return '%s' % (self.id)
        
