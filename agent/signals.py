import json

from django.db.models.signals import post_save
from django.dispatch import receiver
from agent.models import Agent
from .agents import agent

# method for updating
@receiver(post_save, sender=Agent)
def update_stock(sender, **kwargs):
    magent = agent.MUSAAgent(id_agent=kwargs['instance'].id, topic=kwargs['instance'].topic)
    magent.updateAgentConfig(data=kwargs['instance'].settings)
    