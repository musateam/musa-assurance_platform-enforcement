#!/usr/bin/env python
import threading, logging, time

from kafka import KafkaConsumer, KafkaProducer


class Producer(threading.Thread):
    daemon = True

    def run(self):
        producer = KafkaProducer(
            bootstrap_servers='37.48.247.124:9092',
            security_protocol='SSL',
            ssl_check_hostname=False,
            ssl_cafile='/home/urkizu/Projects/musa/musa-assurance_platform-enforcement_demo/platform/agent/broker/certs/kafka-ca.cert'
        )

        while True:
            producer.send('framework.agents.framework', '{"a":1}')
            time.sleep(1)


class Consumer(threading.Thread):
    daemon = True

    def run(self):
        consumer = KafkaConsumer(
            bootstrap_servers='37.48.247.124:9092',
            security_protocol='SSL',
            ssl_check_hostname=False,
            ssl_cafile='/home/urkizu/Projects/musa/musa-assurance_platform-enforcement_demo/platform/agent/broker/certs/kafka-ca.cert',
            auto_offset_reset='earliest'
        )
        consumer.subscribe(['framework.agents.framework'])

        for message in consumer:
            print (message)


def main():
    threads = [
        Producer(),
        #Consumer()
    ]

    for t in threads:
        t.start()

    time.sleep(10)

if __name__ == "__main__":
    logging.basicConfig(
        format='%(asctime)s.%(msecs)s:%(name)s:%(thread)d:%(levelname)s:%(process)d:%(message)s',
        level=logging.INFO
        )
    main()