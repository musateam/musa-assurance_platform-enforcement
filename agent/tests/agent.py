import os
import sys
import inspect

cmd_folder = os.path.realpath(
    os.path.dirname(
        os.path.abspath(os.path.split(inspect.getfile(inspect.currentframe()))[0])))

if cmd_folder not in sys.path:
    sys.path.insert(0, cmd_folder)

from transitions import Machine
from transitions.extensions import GraphMachine
from IPython.display import Image, display, display_png

class Agent(object):
    def show_graph(self):
        self.graph.draw('state.png', prog='dot')
        display(Image('state.png'))        

transitions = [
    # IDLE -> AWAKED
    {'trigger': 'awake', 'source': 'idle', 'dest': 'awaking', 'after': ['on_change_state', 'on_awaking']},
    {'trigger': 'awaked', 'source': 'awaking', 'dest': 'awaked', 'after': ['on_change_state', 'on_awaked']},
    # AWAKED -> INITTIATED
    {'trigger': 'init', 'source': 'awaked', 'dest': 'inittiating', 'after': ['on_change_state', 'on_inittiating']},
    {'trigger': 'inittiated', 'source': 'inittiating', 'dest': 'inittiated', 'after': ['on_change_state', 'on_inittiated']},
    # INITTIATED -> STARTED
    {'trigger': 'start', 'source': 'inittiated', 'dest': 'starting', 'after': ['on_change_state', 'on_starting']},
    {'trigger': 'started', 'source': 'starting', 'dest': 'started', 'after': ['on_change_state',]},
    # STARTED -> STARTED ( update )
    {'trigger': 'update', 'source': 'started', 'dest': 'updating', 'after': ['on_change_state', 'on_updating']},
    {'trigger': 'started', 'source': 'updating', 'dest': 'started', 'after': ['on_change_state', 'on_updated']},
    # STARTED -> DISABLED
    {'trigger': 'disable', 'source': 'started', 'dest': 'disabling', 'after': ['on_change_state', 'on_disabling']},
    {'trigger': 'disabled', 'source': 'disabling', 'dest': 'disabled', 'after': ['on_change_state',]},
    # DISABLED -> STARTED
    {'trigger': 'enable', 'source': 'disabled', 'dest': 'enabling', 'after': ['on_change_state', 'on_enabling']},
    {'trigger': 'started', 'source': 'enabling', 'dest': 'started', 'after': ['on_change_state']},
    # STARTED -> STOPPED
    {'trigger': 'stop', 'source': 'started', 'dest': 'stopping', 'after': ['on_change_state', 'on_stopping']},
    {'trigger': 'stopped', 'source': 'starting', 'dest': 'stopped', 'after': ['on_change_state']},
    # STOPPED -> STARTED
    {'trigger': 'start', 'source': 'stopped', 'dest': 'starting', 'after': ['on_change_state', 'on_starting']},
    {'trigger': 'started', 'source': 'starting', 'dest': 'started', 'after': ['on_change_state']}
]
states = [
    'idle',
    'awaking', 'awaked',
    'inittiating', 'inittiated',
    'starting', 'started',
    'enabling',
    'disabling', 'disabled',
    'updating',
    'stopping', 'stopped'
]


model = Agent()
machine = GraphMachine(model=model,
                       states=states,
                       transitions=transitions,
                       auto_transitions=False,
                       initial='idle',
                       title="Agent Management",
                       show_conditions=True)
model.show_graph()
