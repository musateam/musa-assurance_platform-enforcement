from transitions import *
from transitions.extensions import GraphMachine
from IPython.display import Image, display, display_png

class Matter(object):
    def is_valid(self):
        return True
    
    def is_not_valid(self):
        return False
    
    def is_also_valid(self):
        return True
    
    # graph object is created by the machine
    def show_graph(self, **kwargs):
        self.get_graph(**kwargs).draw('state.png', prog='dot')
        display(Image('state.png'))


#transitions = [
#    { 'trigger': 'melt', 'source': 'solid', 'dest': 'liquid' },
#    { 'trigger': 'evaporate', 'source': 'liquid', 'dest': 'gas', 'conditions':'is_valid' },
#    { 'trigger': 'sublimate', 'source': 'solid', 'dest': 'gas', 'unless':'is_not_valid' },
#    { 'trigger': 'ionize', 'source': 'gas', 'dest': 'plasma', 
#      'conditions':['is_valid','is_also_valid'] }
#]

# INNITIAL -> IDLE
transitions = [
    {'trigger': 'idle', 'source': 'initial', 'dest': 'idle'},
    {'trigger': 'awake', 'source': 'idle', 'dest': 'awaking'},
    {'trigger': 'awaked', 'source': 'awaking', 'dest': 'awaked'},
    {'trigger': 'init', 'source': ['awaked', 'error'], 'dest': 'inittiating'},
    {'trigger': 'inittiated', 'source': 'inittiating', 'dest': 'inittiated'},
    {'trigger': 'start', 'source': ['stopped''inittiated', 'error'], 'dest': 'starting'},
    {'trigger': 'started', 'source': 'starting', 'dest': 'started'},
    {'trigger': 'stop', 'source': ['started', 'error'], 'dest': 'stopping'},
    {'trigger': 'stopped', 'source': 'stopping', 'dest': 'stopped'},
    {'trigger': 'error', 'source': ['awaking', 'inittiating', 'starting', 'stopping'], 'dest': 'error'},
]
#states=['solid', 'liquid', 'gas', 'plasma']
states=[
    'initial',
    'idle',
    'awaking', 
    'awaked',
    'inittiating', 
    'inittiated',
    'starting', 
    'started',
    'stopping', 
    'stopped',
]

model = Matter()
machine = GraphMachine(model=model, 
                       states=states, 
                       transitions=transitions,
                       initial='initial',
                       auto_transitions=False, 
                       show_auto_transitions=True, # default value is False
                       title="MUSA Enforcementent state machine",
                       show_conditions=True)
model.show_graph()