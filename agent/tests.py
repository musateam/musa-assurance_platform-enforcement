import time
import ssl

from django.test import TestCase

from agent.broker import Queue, Worker

class BrokerTestCase(TestCase):
    jobs = []
    path = '/home/urkizu/Projects/musa_enformcement/EnforcementPlatform/kafka'
    ssl_context = ssl.SSLContext(ssl.PROTOCOL_SSLv23)
    ssl_context.verify_mode = ssl.CERT_REQUIRED
    ssl_context.check_hostname = True
    ssl_context.load_verify_locations(path+'/ca_cert.pem')
    ssl_context.load_cert_chain(path+'/signed_cert.pem', path+'/private_key.no.password.pem')
    protocol = 'SSL'
    #CLOUDKARAFKA_BROKERS=["shuttle-01.srvs.cloudkafka.com:9093","shuttle-02.srvs.cloudkafka.com:9093","shuttle-03.srvs.cloudkafka.com:9093"]
    CLOUDKARAFKA_BROKERS = ["kafka.musa.org:9092"]

    def callback(self, status, job, data, exception, traceback):
        print 'Status {}'.format(status)
        print 'Result {}'.format(data)
        print 'Exception {}'.format(exception)
        print 'Traceback {}'.format(traceback)
        print 'Job UUID: {}'.format(job.id)
        print 'Enqueued at {}'.format(job.timestamp)
        print 'In topic: {}'.format(job.topic)
        print '.'*50
        self.jobs.append(job.id)

    def setUp(self):
        queue = Worker(
            hosts=self.CLOUDKARAFKA_BROKERS,
            #topic='ql3i-default',
            pattern='ql3i-*',
            callback=self.callback,
            security_protocol=None,
            ssl_context=None
        )
        queue.start()
    def test_queue_message(self):
        # Initialize a queue
        q = Queue(
            hosts=self.CLOUDKARAFKA_BROKERS,
            topic='ql3i-default',
            security_protocol=None,
            ssl_context=None
        )

        # Enqueue the function call
        job = q.enqueue({"kilo":1, "mano":2})
        timeout = 0
        while True:
            timeout = timeout + 1
            if timeout == 10:
                raise ValueError('Timeout executing queue callback')
            try:
                if self.jobs.index(job.id) == 1:
                    break
            except Exception:
                pass
            time.sleep(5)

