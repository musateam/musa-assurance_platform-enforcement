#!/usr/bin/env python
import threading, logging, time

from broker import Queue, Worker

BROKERS = {
    'kafka':{
        'prefix': 'musa.',
        'protocol': None,
        'brokers':['kafka.musa.org:9092']
    }
}

class Producer(threading.Thread):
    daemon = True

    def run(self):
        producer = Queue(
            hosts=BROKERS['kafka']['brokers'],
            topic='musa.agents.a1',
            partition=1,
            security_protocol=BROKERS['kafka']['protocol'],
            ssl_context=None
        )

        producer.enqueue({'status': 'awaked'})


class Consumer(threading.Thread):
    daemon = True

    def run(self):
        def callback(status, job, data, exception, traceback):
            print 'Status {}'.format(status)
            print 'Result {}'.format(data)
            print 'Exception {}'.format(exception)
            print 'Traceback {}'.format(traceback)
            print 'Job UUID: {}'.format(job.id)
            print 'Enqueued at {}'.format(job.timestamp)
            print 'In topic: {}'.format(job.topic)
            print '.'*50

        consumer = Worker(
            hosts=BROKERS['kafka']['brokers'],
            callback=callback,
            topic='musa.agents.a1',
            security_protocol=BROKERS['kafka']['protocol'],
            ssl_context=None
        )

def main():
    threads = [
        Producer(),
        Consumer()
    ]

    for t in threads:
        t.start()

    time.sleep(10)

if __name__ == "__main__":
    logging.basicConfig(
        format='%(asctime)s.%(msecs)s:%(name)s:%(thread)d:%(levelname)s:%(process)d:%(message)s',
        level=logging.INFO
        )
    main()