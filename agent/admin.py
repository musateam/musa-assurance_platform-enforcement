import json
from django.contrib import admin
from django.db import models

from agent.models import Agent, StateEntry, ActivityEntry
from agent.agents import agent
from .signals import *

from json_field import JSONField
from jsoneditor.forms import JSONEditor

####################################################
############ Agent configuration ###################
####################################################
def start(self, request, queryset):
    for entry in queryset.values():
        magent = agent.MUSAAgent(id_agent=entry['id'], topic=entry['topic'])
        magent.do_action(action='start')
        self.message_user(request, "Agent %s successfully started." % entry['id'])
start.short_description = "Start selected agent"

def stop(self, request, queryset):
    for entry in queryset.values():
        magent = agent.MUSAAgent(id_agent=entry['id'], topic=entry['topic'])
        magent.do_action(action='stop')
        self.message_user(request, "Agent %s successfully stopped." % entry['id'])
stop.short_description = "Stop selected agent"

from prettyjson import PrettyJSONWidget
from django import forms

class JsonForm(forms.ModelForm):
    class Meta:
        model = Agent
        fields = '__all__'
        widgets = {
            'system': PrettyJSONWidget(attrs={'initial': 'parsed'}),
        }

class AgentAdmin(admin.ModelAdmin):
    #readonly_fields = [f.name for f in Agent._meta.fields]
    readonly_fields = ['state', 'agent_type', 'create_date', 'topic', 'name', 'lastseen']
    date_hierarchy = 'create_date'
    list_display = ['id', 'endpoint', 'state', 'agent_type', 'create_date', 'name', 'lastseen']
    fieldsets = [
        ('Agent Information', {'fields': ['endpoint', 'agent_type', 'name']}),
        ('Agent Date', {'fields': ['create_date']}),
        ('Agent Broker', {'fields': ['topic']}),
        ('Agent Settings', {'fields': ['settings', 'system']}),
    ]
    search_fields = ['id', 'topic']
    list_filter = ('endpoint', )
    actions = [start, stop]
    form = JsonForm
    formfield_overrides = {
        JSONField:{'widget':JSONEditor},
    }

admin.site.register(Agent, AgentAdmin)

####################################################
############ Base configuration ####################
####################################################
class BaseEntryAdmin(admin.ModelAdmin):
    date_hierarchy = 'timestamp'
    list_display = (
        'id', 'agent', 'topic', 
        'action', 'tag', 'timestamp'
    )
    list_display_links = ('id', )
    list_filter = ('agent__endpoint', 'category', 'action', 'tag')
    search_fields = ['action', 'topic']

####################################################
############ Messages configuration ################
####################################################
class ActionEntryManager(models.Manager):
    def get_queryset(self):
        return super(ActionEntryManager, self).get_queryset().filter(category='agent', action='state', tag='action')

class ActionEntry(ActivityEntry):
    objects = ActionEntryManager()
    class Meta:
        proxy = True

class ActionEntryAdmin(BaseEntryAdmin):
    readonly_fields = [f.name for f in ActionEntry._meta.fields]
    list_display = BaseEntryAdmin.list_display + ('partition', 'category', 'direction', 'state', 'trigger', 'data')
    def direction(self, obj):
        return obj.activity.get('direction', 'undefined')
    direction.short_description = 'Message direction'
    def state(self, obj):
        return obj.activity.get('state', 'undefined')
    state.short_description = 'State'
    def trigger(self, obj):
        return obj.activity.get('trigger', 'undefined')
    trigger.short_description = 'Action'
    def data(self, obj):
        return obj.activity.get('data', {})
    data.short_description = 'Action data'

    def suit_cell_attributes(self, obj, column):
        if column == 'data':
            return {'class': 'input-medium'}
admin.site.register(ActionEntry, ActionEntryAdmin)

####################################################
############ State Changes configuration ###########
####################################################
class StateEntryAdmin(BaseEntryAdmin):
    readonly_fields = [f.name for f in StateEntry._meta.fields]
    list_display = BaseEntryAdmin.list_display + ('state', )
    search_fields = BaseEntryAdmin.search_fields + ['state', ]
admin.site.register(StateEntry, StateEntryAdmin)

####################################################
############ All events configuration ##############
####################################################
class ActivityEntryAdmin(BaseEntryAdmin):
    readonly_fields = [f.name for f in ActivityEntry._meta.fields]
    date_hierarchy = 'timestamp'
    list_display = BaseEntryAdmin.list_display + ('category', 'activity', )
    search_fields = BaseEntryAdmin.search_fields + ['state', ]
    def suit_cell_attributes(self, obj, column):
        if column == 'activity':
            return {'class': 'input-medium'}
admin.site.register(ActivityEntry, ActivityEntryAdmin)

####################################################
############ PROXY events configuration ############
####################################################
class ProxyEntryManager(models.Manager):
    def get_queryset(self):
        return super(ProxyEntryManager, self).get_queryset().filter(category='proxy')

class ProxyEntry(ActivityEntry):
    objects = ProxyEntryManager()
    class Meta:
        proxy = True

class ProxyEntryAdmin(BaseEntryAdmin):
    readonly_fields = [f.name for f in ProxyEntry._meta.fields]
    list_display = BaseEntryAdmin.list_display + ('timestamp', 'http_method', 'protocol', 'hostname', 'url', 'ip', )#'headers',)
    def http_method(self, obj):
        return obj.activity.get('method', 'undefined')
    http_method.short_description = 'HTTP Method'
    def protocol(self, obj):
        return obj.activity.get('protocol', 'undefined')
    protocol.short_description = 'HTTP Protocol'
    def hostname(self, obj):
        return obj.activity.get('hostname', 'undefined')
    hostname.short_description = 'Hostname'
    def url(self, obj):
        return obj.activity.get('url', 'None')
    url.short_description = 'Url'
    def origin(self, obj):
        return obj.activity.get('origin', 'None')
    protocol.origin_description = 'HTTP Protocol'
    def headers(self, obj):
        return obj.activity.get('headers', 'None')
    headers.short_description = 'Headers'
    def ip(self, obj):
        return obj.activity.get('ip', 'None')
    ip.short_description = 'Origin IP'

    list_display_links = ('id', )
    list_filter = ('agent__endpoint', 'category', 'action', 'tag', 'partition',)
    search_fields = ['action', 'topic']

    def suit_cell_attributes(self, obj, column):
        if column == 'headers':
            return {'class': 'input-medium'}
admin.site.register(ProxyEntry, ProxyEntryAdmin)

####################################################
############ IDM configuration ################
####################################################
class IDMEntryManager(models.Manager):
    def get_queryset(self):
        return super(IDMEntryManager, self).get_queryset().filter(category='idm')

class IDMEntry(ActivityEntry):
    objects = IDMEntryManager()
    class Meta:
        proxy = True

class IDMEntryAdmin(ProxyEntryAdmin):
    readonly_fields = [f.name for f in IDMEntry._meta.fields]
    list_display = ProxyEntryAdmin.list_display + ('provider', 'user',)
    def provider(self, obj):
        return obj.activity.get('provider', 'undefined')
    provider.short_description = 'Provider Name'
    def user(self, obj):
        user = obj.activity.get('user', None)
        if not user:
            return 'undefined'
        return user.get('userID')
    user.short_description = 'User'


admin.site.register(IDMEntry, IDMEntryAdmin)

####################################################
############ Access Control configuration ##########
####################################################
class ACEntryManager(models.Manager):
    def get_queryset(self):
        return super(ACEntryManager, self).get_queryset().filter(category='ac')

class ACEntry(ActivityEntry):
    objects = ACEntryManager()
    class Meta:
        proxy = True

class ACEntryAdmin(ProxyEntryAdmin):
    readonly_fields = [f.name for f in ACEntry._meta.fields]
    list_display = ProxyEntryAdmin.list_display + ('result', )
    def result(self, obj):
        return obj.activity.get('result', 'undefined')
    result.short_description = 'Decision result'

admin.site.register(ACEntry, ACEntryAdmin)

############ Access Control configuration ##########
####################################################
class LogEntryManager(models.Manager):
    def get_queryset(self):
        return super(LogEntryManager, self).get_queryset().filter(category='log')

class LogEntry(ActivityEntry):
    objects = LogEntryManager()
    class Meta:
        proxy = True

class LogEntryAdmin(BaseEntryAdmin):
    readonly_fields = [f.name for f in LogEntry._meta.fields]
    list_display = BaseEntryAdmin.list_display + ('msg', )
    def msg(self, obj):
        return obj.activity.get('msg', 'undefined')
    msg.short_description = 'Log message'

admin.site.register(LogEntry, LogEntryAdmin)