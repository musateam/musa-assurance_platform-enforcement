from __future__ import unicode_literals

from django.apps import AppConfig

from agents.manager import AgentManager

class AgentConfig(AppConfig):
    name = 'agent'
    AppConfig.status = 'inittiated'
    aManager = AgentManager()
    def ready(self):
        if self.status is 'started':
            return

        self.aManager.start()
        self.status = 'started'
        print "Enforcement Platform ready"
        