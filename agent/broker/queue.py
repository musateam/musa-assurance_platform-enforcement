from __future__ import absolute_import, print_function, unicode_literals

import logging
import time
import uuid
import dill
import kafka
import simplejson as json

from .job import Job


class Queue(object):
    """KQ queue.

    A queue serializes incoming function calls and places them into a Kafka
    topic as *jobs*. Workers fetch these jobs and execute them asynchronously
    in the background. Here is an example of initializing and using a queue:

    .. code-block:: python

        from kq import Queue, Job

        queue = Queue(
            hosts='host:7000,host:8000',
            topic='foo',
            compression='gzip',
            acks=0,
            retries=5,
            job_size=10000000,
            cafile='/my/files/cafile',
            certfile='/my/files/certfile',
            keyfile='/my/files/keyfile',
            crlfile='/my/files/crlfile'
        )
        job = queue.enqueue(my_func, *args, **kwargs)
        assert isinstance(job, Job)

    .. note::

        The number of partitions in a Kafka topic limits how many workers
        can read from the queue in parallel. For example, maximum of 10
        workers can work off a queue with 10 partitions.


    :param hosts: Comma-separated Kafka hostnames and ports. For example,
        ``"localhost:9000,localhost:8000,192.168.1.1:7000"`` is a valid input
        string. Default: ``"127.0.0.1:9092"``.
    :type hosts: str | unicode
    :param topic: Name of the Kafka topic. Default: ``"default"``.
    :type topic: str | unicode
    :param compression: The algorithm used for compressing job data. Allowed
        values are: ``"gzip"``, ``"snappy"`` and ``"lz4"``. Default: ``None``
        (no compression).
    :type compression: str | unicode
    :param acks: The number of acknowledgements required from the broker(s)
        before considering a job successfully enqueued. Allowed values are:

        .. code-block:: none

            0: Do not wait for any acknowledgment from the broker leader
               and consider the job enqueued as soon as it is added to
               the socket buffer. Persistence is not guaranteed on broker
               failures.

            1: Wait for the job to be saved on the broker leader but not
               for it be replicated across other brokers. If the leader
               broker fails before the replication finishes the job may
               not be persisted.

           -1: Wait for the job to be replicated across all brokers. As
               long as one of the brokers is functional job persistence
               is guaranteed.

        Default: ``1``.

    :type acks: int
    :param retries: The maximum number of attempts to re-enqueue a job when
        the job fails to reach the broker. Retries may alter the sequence of
        the enqueued jobs. Default: ``0``.
    :type retries: int
    :param job_size: The max size of each job in bytes. Default: ``1048576``.
    :type job_size: int
    :param cafile: Full path to the trusted CA certificate file.
    :type cafile: str | unicode
    :param certfile: Full path to the client certificate file.
    :type certfile: str | unicode
    :param keyfile: Full path to the client private key file.
    :type keyfile: str | unicode
    :param crlfile: Full path to the CRL file for validating certification
        expiry. This option is only available with Python 3.4+ or 2.7.9+.
    :type crlfile: str | unicode
    """

    def __init__(self,
                 hosts='127.0.0.1:9092',
                 topic='default',
                 compression=None,
                 acks=1,
                 retries=0,
                 job_size=1048576,
                 partition=0,
                 security_protocol=None,
                 ssl_context=None,
                 check_hostname=None,
                 cafile=None,
                 certfile=None,
                 keyfile=None,
                 crlfile=None):
        self._hosts = hosts
        self._topic = topic
        self._partition = partition
        self._logger = logging.getLogger('musa')
        self._producer = kafka.KafkaProducer(
            bootstrap_servers=self._hosts,
            compression_type=compression,
            acks=acks,
            retries=retries,
            max_request_size=job_size,
            buffer_memory=max(job_size, 33554432),
            security_protocol=security_protocol,
            ssl_check_hostname=check_hostname,
            ssl_context=ssl_context,
            ssl_cafile=cafile,
            ssl_certfile=certfile,
            ssl_keyfile=keyfile,
            ssl_crlfile=crlfile,
#            value_serializer=lambda v: json.dumps(v).encode('utf-8')
        )


    def __repr__(self):
        """Return a string representation of the queue.

        :return: String representation of the queue.
        :rtype: str | unicode
        """
        return 'Queue(topic={})'.format(self._topic)

    @property
    def producer(self):
        """Return the Kafka producer object.

        :return: Kafka producer object.
        :rtype: kafka.producer.KafkaProducer
        """
        return self._producer

    @property
    def hosts(self):
        """Return the list of Kafka host names and ports.

        :return: List of Kafka host names and ports.
        :rtype: [str]
        """
        return self._hosts.split(',')

    @property
    def topic(self):
        """Return the name of the Kafka topic in use.

        :return: Name of the Kafka topic in use.
        :rtype: str | unicode
        """
        return self._topic

    def enqueue(self, obj, topicData={}):
        """Place the function call (or the job) in the Kafka topic.

        For example:

        .. code-block:: python

            import requests
            from kq import Queue

            q = Queue()

            # You can queue the JSON
            job = q.enqueue({midata: 1, kil: foo})

            # Or you can queue a kq.job.Job instance directly
            q.enqueue(job)

        :param obj: JSON or the job object to enqueue.
        :type obj: callable | kq.job.Job
        :return: The job that was enqueued
        :rtype: kq.job.Job
        """
        if isinstance(obj, Job):
            data = obj.data
            key = obj.key
        else:
            data = obj
            key = None

        topicToSend = self._topic
        if topicData.get('topic'):
            topicToSend = topicData['topic']

        partitionToSend = self._partition
        if topicData.get('partition'):
            partitionToSend = topicData['partition']

        job = Job(
            id=str(uuid.uuid4()),
            timestamp=int(time.time()),
            topic=topicToSend,
            data=data,
            key=key
        )
        #print("Topic %s Partition %d Job %s key %s" % (topicToSend, partitionToSend, job, key))
        self._producer.send(topicToSend, json.dumps(job), key=key, partition=partitionToSend)
        self._logger.info('Enqueued: {}'.format(job))
        return job

    def enqueue_with_key(self, key, obj):
        """Place the function call (or the job) in the Kafka topic with key.

        For example:

        .. code-block:: python

            import requests
            from kq import Queue

            q = Queue()

            # You can queue the function call with its arguments
            job = q.enqueue_with_key('my_key', {midata: 1, kil: foo})

            # Or you can queue a kq.job.Job instance directly
            q.enqueue_with_key('my_key', job)

        :param key: The key for the Kafka message. Jobs with the same key are
            guaranteed to be placed in the same Kafka partition and processed
            sequentially. If a job object is enqueued, its key is overwritten.
        :type key: str
        :param obj: JSON or the job object to enqueue.
        :type obj: callable | kq.job.Job
        :return: The job that was enqueued
        :rtype: kq.job.Job
        """
        if isinstance(obj, Job):
            data = obj.data
        else:
            data = obj

        job = Job(
            id=str(uuid.uuid4()),
            timestamp=int(time.time()),
            topic=self._topic,
            data=data,
            key=key
        )
        self._producer.send(self._topic, dill.dumps(job), key=key)
        self._logger.info('Enqueued: {}'.format(job))
        return job

    def flush(self):
        """Force-flush all buffered records to the broker."""
        self._logger.info('Flushing {} ...'.format(self))
        self._producer.flush()
