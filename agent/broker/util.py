
import ssl

from django.conf import settings

def get_SSL_Context():
    if settings.BROKERS['kafka']['protocol'] is None:
        return None

    ssl_context = ssl.SSLContext(ssl.PROTOCOL_SSLv23)

    ssl_context.verify_mode = ssl.CERT_REQUIRED
    ssl_context.check_hostname = True
    ssl_context.load_verify_locations(settings.BROKERS['kafka']['ssl']['ca_pem'])
    ssl_context.load_cert_chain(settings.BROKERS['kafka']['ssl']['cert_pem'], settings.BROKERS['kafka']['ssl']['private_key'])

    return ssl_context
