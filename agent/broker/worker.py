from __future__ import absolute_import, print_function, unicode_literals

import threading
import logging

import traceback as tb

import simplejson as json
import kafka

from .job import Job

def rec_repr(record):
    """Return the string representation of the consumer record.

    :param record: Record fetched from the Kafka topic.
    :type record: kafka.consumer.fetcher.ConsumerRecord
    :return: String representation of the consumer record.
    :rtype: str | unicode
    """
    return 'Record(topic={}, partition={}, offset={})'.format(
        record.topic, record.partition, record.offset
    )

class Worker(threading.Thread):
    """KQ worker.

    A worker fetches jobs from a Kafka broker, de-serializes them and
    executes them asynchronously in the background. Here is an example
    of initializing and starting a worker:

    .. code-block:: python

        from kq import Worker

        worker = Worker(
            hosts='host:7000,host:8000',
            topic='foo',
            callback=None,
            job_size=10000000,
            cafile='/my/files/cafile',
            certfile='/my/files/certfile',
            keyfile='/my/files/keyfile',
            crlfile='/my/files/crlfile'
        )
        worker.start()

    .. note::

        The number of partitions in a Kafka topic limits how many workers
        can read from the queue in parallel. For example, maximum of 10
        workers can work off a queue with 10 partitions.

    :param hosts: Comma-separated Kafka hostnames and ports. For example,
        ``"localhost:9000,localhost:8000,192.168.1.1:7000"`` is a valid
        input string. Default: ``"127.0.0.1:9092"``.
    :type hosts: str | unicode
    :param topic: Name of the Kafka topic. Default: ``"default"``.
    :type topic: str | unicode
    :param callback: Function executed after a job is fetched and processed.
        Its signature must be ``callback(status, job, exception, traceback)``
        where:

        .. code-block:: none

            status (str | unicode)
                The status of the job execution, which can be "failure" or "success".

            job (kq.job.Job)
                The job namedtuple object consumed by the worker.

            result (object)
                The result of the job execution.

            exception (Exception | None)
                The exception raised while the job was running, or None
                if there were no errors.

            traceback (str | unicode | None)
                The traceback of the exception raised while the job was
                running, or None if there were no errors.

    :type callback: callable
    :param job_size: The max size of each job in bytes. Default: ``1048576``.
    :type job_size: int
    :param cafile: Full path to the trusted CA certificate file.
    :type cafile: str | unicode
    :param certfile: Full path to the client certificate file.
    :type certfile: str | unicode
    :param keyfile: Full path to the client private key file.
    :type keyfile: str | unicode
    :param crlfile: Full path to the CRL file for validating certification
        expiry. This option is only available with Python 3.4+ or 2.7.9+.
    :type crlfile: str | unicode
    """
    daemon = True

    def __init__(self,
                 hosts='127.0.0.1:9092',
                 topic=None,
                 pattern=None,
                 callback=None,
                 context=None,
                 job_size=1048576,
                 security_protocol=None,
                 ssl_context=None,
                 check_hostname=True,
                 cafile=None,
                 certfile=None,
                 keyfile=None,
                 crlfile=None):
        self._hosts = hosts
        self._topic = topic
        self._pattern = pattern
        self._callback = callback
        self._context = context
        self._options = {
            'job_size': job_size
        }
        self._ssl = {
            'security_protocol': security_protocol,
            'ssl_context': ssl_context,
            'check_hostname': check_hostname,
            'cafile': cafile,
            'certfile': certfile,
            'keyfile': keyfile,
            'crlfile': crlfile
        }
        self._pool = None
        self._logger = logging.getLogger('musa')
        threading.Thread.__init__(self)

    def __del__(self):
        """Commit the Kafka consumer offsets and close the consumer."""
        if hasattr(self, '_consumer'):
            try:
                self._logger.info('Closing consumer ...')
                self._consumer.close()
            except Exception as e:  # pragma: no cover
                self._logger.warning('Failed to close consumer: {}'.format(e))

    def __repr__(self):
        """Return a string representation of the worker.

        :return: string representation of the worker
        :rtype: str | unicode
        """
        return 'Worker(topic={}, pattern={})'.format(self._topic, self._pattern)

    def _exec_callback(self, status, data, record, exception, traceback):
        """Execute the callback in a try-except block.

        :param status: The status of the job consumption. Possible values are
            ``failure`` and ``success``.
        :type status: str | unicode
        :param job: The job consumed by the worker
        :type job: kq.job.Job
        :type result: object
        :param exception: Exception raised while the job was running (i.e.
            status was ``failure``), or ``None`` if there were no errors
            (i.e. status was ``success``).
        :type exception: Exception | None
        :param traceback: The stacktrace of the exception (i.e. status was
            ``failure``) was running or ``None`` if there were no errors.
        :type traceback: str | unicode | None
        """
        if self._callback is not None:
            def execute_callback( ):
                try:
                    self._logger.info('Executing callback ...')
                    self._callback(status, data, record, exception, traceback)
                except Exception as e:
                    self._logger.exception('Callback failed: {}'.format(e))
                    execute_callback()
            execute_callback()

    def _consume_record(self, record):
        """De-serialize the message and execute the incoming job.

        :param record: Record fetched from the Kafka topic.
        :type record: kafka.consumer.fetcher.ConsumerRecord
        """
        if record.partition != 1:
            self._logger.debug('Message {} discarted'.format(record))
            return

        rec = rec_repr(record)
        self._logger.info('Processing {} ...'.format(rec))
        try:
            jsonJob = json.loads(record.value)
            job = Job(
                id=jsonJob['id'],
                timestamp=jsonJob['timestamp'],
                topic=jsonJob['topic'],
                data=jsonJob['data'],
                key=jsonJob['key']
            )
        except:
            self._logger.warning('{} unloadable. Skipping ...'.format(rec))
        else:
            try:
                # Simple check for job validity
                if not isinstance(job, Job):
                    self._logger.warning('{} malformed. Skipping ...'.format(rec))
                    return
                data = job.data
                self._logger.info('Running Job {}: {} ...'.format(
                    job.id, data
                ))
            except Exception as e:
                self._logger.exception('Job {} failed: {}'.format(job.id, e))
                self._exec_callback('failure', data, record, e, tb.format_exc())
            else:
                self._logger.info('Job {} returned: {}'.format(job.id, data))
                self._exec_callback('success', data, record, None, None)

    @property
    def consumer(self):
        """Return the Kafka consumer object.

        :return: Kafka consumer object.
        :rtype: kafka.consumer.KafkaConsumer
        """
        return self._consumer

    @property
    def hosts(self):
        """Return the list of Kafka host names and ports.

        :return: list of Kafka host names and ports
        :rtype: [str]
        """
        return self._hosts.split(',')

    @property
    def topic(self):
        """Return the name of the Kafka topic in use.

        :return: Name of the Kafka topic in use.
        :rtype: str | unicode
        """
        return self._topic

    def run(self):
        """Start fetching and processing enqueued jobs in the topic.

        Once started, the worker will continuously poll the Kafka broker in
        a loop until jobs are available in the topic partitions. The loop is
        only stopped via external triggers (e.g. keyboard interrupts).
        """
        self._logger.info('Starting {} ...'.format(self))
        #def backgroundQueue(self):
        #    try:
        #        for record in self._consumer:
        #            self._consume_record(record)
        #            self._consumer.commit()
        #    except KeyboardInterrupt:  # pragma: no cover
        #        self._logger.info('Stopping {} ...'.format(self))
        #self._pool.map(backgroundQueue, self)

        try:
            self._consumer = kafka.KafkaConsumer(
                #group_id=self._topic,
                bootstrap_servers=self._hosts,
                max_partition_fetch_bytes=self._options['job_size'] * 2,
                security_protocol=self._ssl['security_protocol'],
                ssl_check_hostname=self._ssl['check_hostname'],
                ssl_context=self._ssl['ssl_context'],
                ssl_cafile=self._ssl['cafile'],
                ssl_certfile=self._ssl['certfile'],
                ssl_keyfile=self._ssl['keyfile'],
                ssl_crlfile=self._ssl['crlfile'],
                group_id='musa',
    #            value_deserializer=lambda v: json.loads(v.decode('utf-8')),
                max_poll_records=50,
                session_timeout_ms=90000,
                consumer_timeout_ms=-1,
                enable_auto_commit=False,
                auto_offset_reset='earliest',
            )
            self._consumer.subscribe(self._topic, self._pattern)
            self._logger.info('Worker host {} topic {} pattern {}'.format(
                self._hosts, self._topic, self._pattern))
            for record in self._consumer:
                self._consume_record(record)
                self._consumer.commit()
        except KeyboardInterrupt:  # pragma: no cover
            self._logger.info('Stopping {} ...'.format(self))

