from __future__ import absolute_import, print_function, unicode_literals

from collections import namedtuple


# Namedtuple which encapsulates a KQ job
Job = namedtuple(
    typename='Job',
    field_names=[
        'id',         # UUID of the msg
        'timestamp',  # Unix timestamp indicating when the msg was queued
        'topic',      # Name of the Kafka topic the msg was enqueued in
        'data',       # msg JSON data
        'key'         # msgs w/ the same keys end up in the same partition
    ]
)
Job.__new__.__defaults__ = (None,)
