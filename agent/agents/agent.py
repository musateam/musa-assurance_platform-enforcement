import logging
import json

from django.conf import settings
from django.db import models
from django.utils import timezone
from transitions import Machine, logger
from enforcement.models import Endpoint

from ..models import Agent, StateEntry, ActivityEntry
from ..broker import Queue, util


class NewManager(models.Manager):
    # ...
    pass


class MAgent(Agent):
    objects = NewManager()
    logger = None
    queue = None
    partition = 0

    class Meta:
        proxy = True

    def sendMessage(self, msg):
        category = msg.get('category', 'None')
        action = msg.get('action', 'None')
        tag = msg.get('tag', 'None')
        value = msg.get('value', {})
        # Save msg
        value['direction'] = 'out'
        activity_entry = ActivityEntry(
            category=category, action=action, tag=tag,
            agent=self, topic=self.topic, partition=self.partition,
            activity=json.dumps(value)
        )
        activity_entry.save()
        self.queue.enqueue(msg, {'topic': self.topic, 'partition': self.partition})

    def on_idle(self, data):
        event_data = data.args[0]
        # agent system settings
        self.system = event_data.pop('system', None)
        # agent current settings
        self.settings = event_data
        # agent type
        self.agent_type = event_data['agent']['options']['type']
        # save
        self.save()
        # set agent to intermediate state
        self.awake()

    def on_awaking(self, data={}):
        self.sendMessage({'category': 'agent', 'action': 'state', 'tag':'action', 'value': {'trigger': 'awake'}})

    def on_awaked(self, data={}):
        self.init()

    def on_inittiating(self, data={}):
        url = None
        if getattr(self, 'endpoint') != None:
            url = self.endpoint.url
        self.sendMessage({'category': 'agent', 'action': 'state', 'tag':'action', 'value': {'trigger': 'init', 'data': json.dumps({ 'proxy': {'remote': url } })}})

    def on_inittiated(self, data={}):
        print "Inititted"
        self.start()

    def on_starting(self, data={}):
        self.sendMessage({'category': 'agent', 'action': 'state', 'tag':'action', 'value': {'trigger': 'start'}})

    def on_disabling(self, data={}):
        self.sendMessage({'category': 'agent', 'action': 'state', 'tag':'action', 'value': {'trigger': 'disable'}})

    def on_enabling(self, data={}):
        self.sendMessage({'category': 'agent', 'action': 'state', 'tag':'action', 'value': {'trigger': 'enable'}})

    def on_stopping(self, data={}):
        self.sendMessage({'category': 'agent', 'action': 'state', 'tag':'action', 'value': {'trigger': 'stop'}})

    def on_change_state(self, data={}):
        self.save()
        self.logger.debug('Agent {} is now {} data {}'.format(self.id, self.state, data))
        state_entry = StateEntry(
            category='agent', action='state', tag='set',
            agent=self, topic=self.topic, partition=self.partition,
            state=self.state
        )
        state_entry.save()

    def updated(self, event_data):
        # agent settings
        self.settings = event_data
        # agent type
        self.agent_type = event_data['agent']['options']['type']
        # save
        self.save()

class MUSAAgent(object):

    def __init__(self, topic, id_endpoint=-1, id_agent=-1, partition=0):
        self.logger = logging.getLogger('musa')
        self.queue = Queue(
            hosts=settings.BROKERS['kafka']['brokers'],
            security_protocol=settings.BROKERS['kafka']['protocol'],
            check_hostname=settings.BROKERS['kafka']['ssl']['check_hostname'],
            cafile=settings.BROKERS['kafka']['ssl']['cafile'],
            certfile=settings.BROKERS['kafka']['ssl']['certfile'],
            keyfile=settings.BROKERS['kafka']['ssl']['keyfile'],
            crlfile=settings.BROKERS['kafka']['ssl']['crlfile']
            #ssl_context=util.get_SSL_Context()
        )
        # Transitions log level set
        logger.setLevel(logging.DEBUG)
        self.transitions = [
            # INNITIAL -> IDLE
            {'trigger': 'idle', 'source': '*', 'dest': 'idle', 'after': ['on_change_state', 'on_idle']},
            # IDLE -> AWAKED
            {'trigger': 'awake', 'source': 'idle', 'dest': 'awaking', 'after': ['on_change_state', 'on_awaking']},
            {'trigger': 'awaked', 'source': 'awaking', 'dest': 'awaked', 'after': ['on_change_state', 'on_awaked']},
            # AWAKED -> INITTIATED
            {'trigger': 'init', 'source': 'awaked', 'dest': 'inittiating', 'after': ['on_change_state', 'on_inittiating']},
            {'trigger': 'inittiated', 'source': 'inittiating', 'dest': 'inittiated', 'after': ['on_change_state', 'on_inittiated']},
            # INITTIATED -> STARTED
            {'trigger': 'start', 'source': 'inittiated', 'dest': 'starting', 'after': ['on_change_state', 'on_starting']},
            {'trigger': 'started', 'source': 'starting', 'dest': 'started', 'after': ['on_change_state',]},
            # STARTED -> STOPPED
            {'trigger': 'stop', 'source': 'started', 'dest': 'stopping', 'after': ['on_change_state', 'on_stopping']},
            {'trigger': 'stopped', 'source': 'stopping', 'dest': 'stopped', 'after': ['on_change_state']},
            # STOPPED -> STARTED
            {'trigger': 'start', 'source': 'stopped', 'dest': 'starting', 'after': ['on_change_state', 'on_starting']},
            {'trigger': 'started', 'source': 'starting', 'dest': 'started', 'after': ['on_change_state']}
        ]
        self.states = [
            'initial',
            'idle',
            'awaking', 'awaked',
            'inittiating', 'inittiated',
            'starting', 'started',
            'updating', 'updated',
            'stopping', 'stopped',
        ]
        self.partition = partition
        if id_agent != -1:
            self.agent, created = MAgent.objects.get_or_create(id=id_agent)
        else:
            try:
                self.agent, created = MAgent.objects.get_or_create(
                    endpoint=Endpoint.objects.get(id_endpoint=id_endpoint, name=topic.split('.')[2]))
            except :
                self.agent, created = MAgent.objects.get_or_create(topic=topic, name=topic.split('.')[2])

        if not created:
            self.logger.debug(
                "Agent {} on {} previously found".format(id_agent, id_endpoint))
        else:
            self.logger.debug(
                "Agent {} on {} created".format(id_agent, id_endpoint))
        self.agent.topic = topic
        self.agent.queue = self.queue
        self.agent.logger = self.logger
        self.machine = Machine(self.agent, states=self.states, ignore_invalid_triggers=True,
                               transitions=self.transitions, initial=self.agent.state, send_event=True)
        self.agent.save()

    def do_action(self, action, data={}):
        # Apply transition state
        getattr(self.agent, action)(data)

    def updateAgentConfig(self, data):
        url = None
        if getattr(self.agent, 'endpoint') != None:
            url = self.agent.endpoint.url

        data['proxy']['remote'] = url
        self.agent.sendMessage({'category': 'agent', 'action': 'state', 'tag':'action', 'value': {'trigger': 'update', 'data': json.dumps(data)}})                

    def handle_agent_message(self, category, action, tag, value):
        # TODO: This must be resolved more elegant
        if action == 'state':
            if tag == 'action':                
                trigger = value.get('trigger', 'None')
                data = value.get('data', 'None')
                self.do_action(action=trigger, data=data)
                self.logger.debug('.'*50)
                return
        if action == 'management':
            if tag == 'keepalive':
                self.agent.lastseen = timezone.now()
                self.agent.save()


    def on_message(self, msg, record):
        #KLUDGE: Discard messages on 0 partition, we write on partition 0 and listen for agents on partition 1, partition 3 is for events notification
        #TODO: we need to listen only to partitions 1, 2
        if record.partition == 0:
            self.logger.debug('Message {} discarted'.format(record))
            return

        if record.partition == 1:
            category = msg.get('category', 'None')
            action = msg.get('action', 'None')
            tag = msg.get('tag', 'None')
            value = msg.get('value', {})

            # Save msg
            value['direction'] = 'in'
            activity_entry = ActivityEntry(
                category=category, action=action, tag=tag,
                agent=self.agent, topic=self.agent.topic, partition=self.partition,
                activity=json.dumps(value)
            )
            activity_entry.save()

            # Handle agent messages
            if category == 'agent':
                self.handle_agent_message(category=category, action=action, tag=tag, value=value)
                return

            self.logger.debug('Message {} not processed'.format(record))
            return
