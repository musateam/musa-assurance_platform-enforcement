
import logging

from django.conf import settings

from ..broker import Queue, Worker, util

class AgentManager(object):
    def __init__(self, conf={}):
        self.logger = logging.getLogger('musa')

        self.worker = Worker(
            hosts=settings.BROKERS['kafka']['brokers'],
            #topic=settings.BROKERS['kafka']['prefix']+'agents.e1',
            pattern=settings.BROKERS['kafka']['prefix']+'agents.*',
            callback=self.onAgentMessage,
            security_protocol=settings.BROKERS['kafka']['protocol'],
            check_hostname=settings.BROKERS['kafka']['ssl']['check_hostname'],
            cafile=settings.BROKERS['kafka']['ssl']['cafile'],
            certfile=settings.BROKERS['kafka']['ssl']['certfile'],
            keyfile=settings.BROKERS['kafka']['ssl']['keyfile'],
            crlfile=settings.BROKERS['kafka']['ssl']['crlfile']
            #ssl_context=util.get_SSL_Context()
        )

    def start(self):
        self.status = 'started'
        self.worker.start()


    def onAgentMessage(self, status, job, record, exception, traceback):
        from .agent import MUSAAgent
        #print 'Agent message data {} topic {}'.format(data, record.topic)
        magent = MUSAAgent(id_endpoint=record.topic.split('.')[2], topic=record.topic, partition=record.partition)
        magent.on_message(job, record)
        self.logger.debug('.'*50)
        