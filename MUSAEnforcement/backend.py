from django.conf import settings
from django.contrib.auth.models import User
from django.contrib import auth
from django.utils.deprecation import MiddlewareMixin
from django.utils.functional import SimpleLazyObject

import jwt

BACKEND_SESSION_KEY = '_auth_user_backend'
HASH_SESSION_KEY = '_auth_user_hash'

def get_user_from_Session(request):
    """
    Returns the user model instance associated with the given request session.
    If no user is retrieved an instance of `AnonymousUser` is returned.
    """
    user = None
    try:
        user_id = auth._get_user_session_key(request)
        backend_path = request.session[BACKEND_SESSION_KEY]
    except KeyError:
        pass
    else:
        if backend_path in settings.AUTHENTICATION_BACKENDS:
            backend = auth.load_backend(backend_path)
            user = backend.get_user(user_id)
            # Verify the session
            if hasattr(user, 'get_session_auth_hash'):
                session_hash = request.session.get(HASH_SESSION_KEY)
                session_hash_verified = session_hash and auth.constant_time_compare(
                    session_hash,
                    user.get_session_auth_hash()
                )
                if not session_hash_verified:
                    request.session.flush()
                    user = None

    return user

def get_custom_user(username):
    try:
        if not username:
            username = 'musa'
        user = User.objects.get(username=username)
    except User.DoesNotExist:
        # Create a new user. There's no need to set a password
        # because only the password from settings.py is checked.
        user = User(username=username)
        user.is_staff = True
        user.is_superuser = True
        user.save()
    return user


def get_user(request):
    """ Si hay un user cacheado tiene prioridad """        
    if hasattr(request, '_cached_user'):
        return request._cached_user

    """ Si hay en el query string un user lo tomamos como valido """
    username = request.GET.get('username')
    if username:
        request._cached_user = get_custom_user(username)
        auth.login(request, request._cached_user)
        return request._cached_user

    """ Si hay una cookie con un token lo tomamos como valido """
    token = request.COOKIES.get(settings.JWT['cookie'])
    if token:
        user = jwt.decode(token, settings.JWT['secret'], algorithms=[settings.JWT['algorithm']])
        request._cached_user = get_custom_user(user['sub'])
        auth.login(request, request._cached_user)
        return request._cached_user

    """ Si hay un usuario en session lo cogo de ahi """
    user = get_user_from_Session(request)
    if user:
        request._cached_user = user
        return request._cached_user

    #request._cached_user = get_custom_user('musa')
    #return request._cached_user
    return None   


class AuthenticationMiddleware(MiddlewareMixin):
    def process_request(self, request):
        assert hasattr(request, 'session'), (
            "The Django authentication middleware requires session middleware "
            "to be installed. Edit your MIDDLEWARE%s setting to insert "
            "'django.contrib.sessions.middleware.SessionMiddleware' before "
            "'django.contrib.auth.middleware.AuthenticationMiddleware'."
        ) % ("_CLASSES" if settings.MIDDLEWARE is None else "")
        user = get_user(request)
        if not user:
            return
        request.user = SimpleLazyObject(lambda: get_user(request))


class SessionAuthenticationMiddleware(MiddlewareMixin):
    """
    Formerly, a middleware for invalidating a user's sessions that don't
    correspond to the user's current session authentication hash. However, it
    caused the "Vary: Cookie" header on all responses.

    It's now a shim to allow a single settings file to more easily support
    multiple versions of Django. Will be RemovedInDjango20Warning.
    """
    def process_request(self, request):
        pass
