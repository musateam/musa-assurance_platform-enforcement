from rest_framework import serializers

from enforcement.models import Project, Service, Endpoint
from .magent import AgentSerializer

class EndpointSerializer(serializers.ModelSerializer):
    agents = AgentSerializer(read_only=True)
    class Meta:
        model = Endpoint
        fields = ('id_endpoint', 'url', 'service', 'agents')


class ServiceSerializer(serializers.ModelSerializer):
    endpoints = EndpointSerializer(many=True, read_only=True)

    class Meta:
        model = Service
        fields = ('id_service', 'name', 'endpoints', 'project')


class ProjectSerializer(serializers.ModelSerializer):
    owner = serializers.ReadOnlyField(source='owner.username')
    services = ServiceSerializer(many=True, read_only=True)

    class Meta:
        model = Project
        fields = ('id_project', 'name', 'owner', 'description', 'create_date', 'services')
