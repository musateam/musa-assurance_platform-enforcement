
from agent.models import Agent
from rest.serializers import AgentSerializer
from rest.permissions import IsOwnerOrReadOnly
from rest_framework import permissions

from rest_framework.viewsets import ModelViewSet

class AgentViewSet(ModelViewSet):
    queryset = Agent.objects.all()
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,
                          IsOwnerOrReadOnly,)
    serializer_class = AgentSerializer
