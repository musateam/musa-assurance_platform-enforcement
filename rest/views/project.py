
from enforcement.models import Project, Service, Endpoint
from rest.serializers import ProjectSerializer, ServiceSerializer, EndpointSerializer
from rest.permissions import IsOwnerOrReadOnly
from rest_framework import permissions

from rest_framework.viewsets import ModelViewSet

class ProjectViewSet(ModelViewSet):
    queryset = Project.objects.all()
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,
                          IsOwnerOrReadOnly,)
    serializer_class = ProjectSerializer
    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)

class ServiceViewSet(ModelViewSet):
    queryset = Service.objects.all()
    serializer_class = ServiceSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,
                          IsOwnerOrReadOnly,)

class EndpointViewSet(ModelViewSet):
    queryset = Endpoint.objects.all()
    serializer_class = EndpointSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,
                          IsOwnerOrReadOnly,)
