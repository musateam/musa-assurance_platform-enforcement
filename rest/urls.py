from django.conf.urls import url, include
from rest_framework.urlpatterns import format_suffix_patterns
from rest_framework.routers import SimpleRouter
from rest_framework_swagger.views import get_swagger_view
from rest import views

router = SimpleRouter()
router.register(r'projects', views.ProjectViewSet)
router.register(r'services', views.ServiceViewSet)
router.register(r'endpoints', views.EndpointViewSet)
router.register(r'agents', views.AgentViewSet)

schema_view = get_swagger_view(title='Pastebin API')

urlpatterns = [
    url(r'^$', schema_view),
    url(r'^', include(router.urls)),
    url(r'^users/$', views.UserList.as_view()),
    url(r'^users/(?P<pk>[0-9]+)/$', views.UserDetail.as_view()),
]


urlpatterns = format_suffix_patterns(urlpatterns)
