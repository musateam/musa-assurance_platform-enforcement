
from django.contrib import admin

from .models import Project, Service, Endpoint

class EndpointInline(admin.TabularInline):
    model = Endpoint
    extra = 1

class ServiceInline(admin.TabularInline):
    model = Service
    extra = 1

class ProjectAdmin(admin.ModelAdmin):
    search_fields = ['name']
    list_filter = ['create_date']
    list_display = [f.name for f in Project._meta.fields]
    list_display_links = ('id_project',)
    fieldsets = [
        ('Project Information', {'fields': ['id_project', 'name', 'description']}),
        ('Project Date', {'fields': ['create_date']}),
    ]
    inlines = [ServiceInline]
    def save_model(self, request, obj, form, change):
        obj.owner = request.user
        obj.save()

class ServiceAdmin(admin.ModelAdmin):
    search_fields = ['name']
    list_filter = ['project']
    list_display = [f.name for f in Service._meta.fields]
    list_display_links = ('id_service', 'project')
    inlines = [EndpointInline]

class EndpointAdmin(admin.ModelAdmin):
    search_fields = ['id_endpoint']
    list_filter = ['service']
    list_display = [f.name for f in Endpoint._meta.fields]
    list_display_links = ('id_endpoint', 'service')

admin.site.register(Project, ProjectAdmin)
admin.site.register(Service, ServiceAdmin)
admin.site.register(Endpoint, EndpointAdmin)
