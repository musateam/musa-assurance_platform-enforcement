from django.db import models

from django.utils import timezone

class Project(models.Model):
    id_project = models.CharField(max_length=200, primary_key=True)
    owner = models.ForeignKey('auth.User', related_name='projects', on_delete=models.CASCADE)
    name = models.CharField(max_length=200, unique=True, default='Project name')
    description = models.CharField(max_length=200, default='Project description')
    create_date = models.DateTimeField('date created', default=timezone.now)

    class Meta:
        ordering = (['create_date'])

    def __str__(self):
        return self.name

    def __unicode__(self):
        return '%s' % (self.name)

class Service(models.Model):
    id_service = models.CharField(max_length=200, primary_key=True)
    project = models.ForeignKey(Project, related_name='services', on_delete=models.CASCADE)
    name = models.CharField(max_length=255, default='Service name')

    def __str__(self):
        return self.name

    def __unicode__(self):
        return '%s' % (self.name)

class Endpoint(models.Model):
    id_endpoint = models.CharField(max_length=200, primary_key=True)
    service = models.ForeignKey(Service, related_name='endpoints', on_delete=models.CASCADE)
    url = models.CharField(max_length=255, unique=True)

    def __str__(self):
        return self.id_endpoint

    def __unicode__(self):
        return '%s:%s' % (self.id_endpoint, self.url)
        