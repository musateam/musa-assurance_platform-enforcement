from django.apps import AppConfig


class EnforcementConfig(AppConfig):
    name = 'enforcement'
    def ready(self):
        print "Enforcement ready"
